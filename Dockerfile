
ARG  ARCH=x86_64
ARG  OS=el9

FROM gitlab-registry.cern.ch/atlas-tdaq-software/atlas-dnf5:v5.2.1.0 as Builder
ARG  ARCH=x86_64
ARG  OS=el9
ARG  SW=gcc13-opt
ARG  RELEASE=tdaq-12-00-00
ARG  LCG=106

ENV  DNF_VAR_PREFIX=/sw/atlas
RUN  dnf5 install -y --enable-repo tdaq-updates --setvar LCG=${LCG} --setvar ARCH=${ARCH} ${RELEASE}_${ARCH}-${OS}-${SW}
RUN  cd ${DNF_VAR_PREFIX}/tdaq && ln -sf ${RELEASE} prod
RUN  mkdir -p /sw/tdaq/ipc

RUN  mkdir -p /sw/tdaq/tools/${ARCH}-${OS}/CMake/3.31.0
RUN  curl https://cmake.org/files/v3.31/cmake-3.31.0-linux-${ARCH}.tar.gz | tar -C /sw/tdaq/tools/${ARCH}-${OS}/CMake/3.31.0  --strip-components=1 -zxf -

FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:${ARCH}-${OS}
ENV  TDAQ_IPC_INIT_REF=file:/sw/tdaq/ipc/init.ref
COPY --from=Builder /sw/atlas /sw/atlas
COPY --from=Builder /sw/tdaq/ipc /sw/tdaq/ipc
COPY --from=Builder /sw/tdaq/tools /sw/tdaq/tools
