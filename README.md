# ATLAS TDAQ Release Containers

This project builds ATLAS TDAQ release containers
including the full software stack.

Containers are multi arch, supporting both x86_64 and
aarch64 architectures. Typically only the optimized
variant of the default configuration is provided by default,
but the Dockerfile can be used to generate any variant.

## Usage

To use the container with the IGui:

```shell
podman run -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY -h $HOSTNAME --rm registry.cern.ch/atlas-tdaq/tdaq:11.2.0
cd
cm_setup
pm_part_hlt.py -p test
setup_daq test.data.xml initial
setup_daq test.data.xml test
```

You should get the IGui for the `test` partition on your screen.

## Modifying the LCG and TDAQ installations in the image

Although the initial installation history is not preserved, the RPM
database is available and you can install additional software

```shell
podman run -it --rm registry.cern.ch/atlas-tdaq/tdaq:11.2.0
cd
curl -L https://gitlab.cern.ch/api/v4/projects/141622/packages/generic/atlas-dnf5/v5.1.9/atlas-dnf5-x86_64-v5.1.9.tar.gz | tar zxf -
alias atlas-dnf5=$(pwd)/atlas-dnf5-x86_64-v5.1.9/atlas-dnf5
export ATLAS_DNF5_INSTALL_DIR=/sw/atlas
atlas-dnf5 list --installed
atlas-dnf5-list LCG_104c_*
atlas-dnf5 install ...
```

## Advanced

This is typically only used by the TDAQ librarian since you need
access to the registry. But you can look at the existing
files if necessary (see [.gitlab-ci.yml]() and [manifest.yml.in]()
and adapt them for your needs.

To generate a version with different release, os or compiler
setting override the RELEASE, OS and SW variables in
the gitlab CI pipeline settings. Note that the final
multi arch image will always be called tdaq:<version>
It is supposed to contain the default version of the release.

If you want to build another variant (e.g. dbg, or for
a different base OS), set the `SUFFIX` variable which will
be appended to the final image name. E.g. `SUFFIX=/debug`
will lead to a

    registry.cern.ch/atlas-tdaq/tdaq/debug:11.2.0

image tag.
